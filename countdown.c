///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date from a given reference time
//
// Example:
//   Reference time: Tue Jan 21 04:26:07 PM HST 2014
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 57
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 58
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 59
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 0
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 1
//
// @author Jared Inouye <jinouye7@hawaii.edu>
// @date   17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
//#include <struct_tm.h>

void convertTime(double secDiff) {
   
   int secInMin = 60;
   int secInHour = 60 * secInMin;
   int secInDay = 24 * secInHour;
   int secInYear = 365 * secInDay;

   // converts secs into terms of years, days, hours, mins, and secs
   long yearDiff = (long) secDiff / secInYear;
   secDiff = secDiff - (yearDiff*secInYear);
   
   long dayDiff = (long) secDiff / secInDay;
   secDiff = secDiff - (dayDiff*secInDay);
   
   long hourDiff = (long) secDiff / secInHour;
   secDiff = secDiff - (hourDiff*secInHour);
   
   long minDiff = (long) secDiff / secInMin;
   secDiff = secDiff - (minDiff*secInMin);

   printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %.f\n", yearDiff, dayDiff, hourDiff, minDiff, secDiff); 
  

}


int main() {

   char buffer [200];

   tzset(); // sets the timezone stuff
   struct tm referenceTime = { 7, 26, 16, 21, 0, 114, 2, 20, -1};
   
   // converts struct tm into a data string and prints
   strftime(buffer, 200, "Reference time: %a %b %d %r %Z %G\n", &referenceTime);
   puts(buffer);

   time_t refTime = mktime(&referenceTime);
   time_t now = time(&now);

   // calculates the difference between the current time and reference time
   double secDiff = difftime(now,refTime);

   while(1) {

      convertTime(secDiff);
      secDiff = secDiff - 1; //count down by sec

      sleep(1); //pauses for one sec

   }

   return 0;

}
